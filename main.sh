#!/bin/bash
source func.sh

menu() {
	echo "1. Exibir linha específica"
	echo "2. Exibir coluna específica."
}

echo""
read -p "Arquivo: " arquivo
echo""

menu

echo ""
read -p "Opção: " opcao
echo ""

case $opcao in
	1)read -p "Linha: " line
	       linha "$line" "$arquivo" ;;
        2)read -p "Coluna: " colum
               coluna "$colum" "$arquivo";;
        *)echo "Opção inválida." ;;
esac
