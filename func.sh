#!/bin/bash

linha() {
	if [ "$#" -ne 2 ]; then
		echo "Uso: linha <numero> <arquivo>"
		return 1
	fi

	local linha="$1"
	local arquivo="$2"

	if [ ! -f "$arquivo" ]; then
		echo "Arquivo não foi encontrado."
		return 1
	fi

	local conteudo=$(sed -n "${linha}p" "$arquivo")

	if [ -z "$conteudo" ]; then
		echo "Linha não encontrada."
		return 1
	fi

	echo "$conteudo"
}

coluna() {
	if [ "$#" -ne 2 ]; then
		echo "Uso: coluna <numero> <arquivo>"
		return 1
	fi

	local coluna="$1"
	local arquivo="$2"

	if [ ! -f "$arquivo" ]; then
		echo "Arquivo não encontrado."
		return 1
	fi

	if [ "$coluna" -le 0 ]; then
		echo "Coluna inválida."
		return 1
	fi

	local conteudo=$(cut -d':' -f"${coluna}" "$arquivo")

	if [ -z "$conteudo" ]; then
		echo "Coluna não encontrada."
		return 1
	fi

	echo "$conteudo"
}

